<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subscription_payment extends Model
{
    protected $table="subscription_payment";

    public function subscription()
    {
        return $this->belongsTo('App\subscription');
    }
}
