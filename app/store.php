<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class store extends Model
{
    protected $table='store';
    protected $primaryKey='store_id';

    public function subscription()
    {
        return $this->hasMany('App\subscription','store_id');
    }

    public function store_products()
    {
        return $this->hasMany('App\store_products','store_id');
    }

  
}
