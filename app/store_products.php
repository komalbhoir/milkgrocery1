<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class store_products extends Model
{
    protected $table='store_products';
    protected $primaryKey='p_id';
   

    public function store()
    {
        return $this->belongsTo('App\store','store_id');
    }
    
}
