<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\address;
use App\subscription;
use App\subscription_payment;
use App\freedeliverycart;

use Carbon\Carbon;

class SubscriptionController extends Controller
{
   public function subscription(Request $request)
    {  
      $current = Carbon::now();
      $user_id= $request->user_id;
       
       $store_id = $request->store_id;
       
       $product_id= $request->product_id;
       
        $product_quantity= $request->quantity;
        $repeat_on= $request->repeat_on;
       if($repeat_on=="one time delivery")
       {
           $delivery_start_date = $current;
           $delivery_end_date = $current;
       }
       else if($repeat_on=="Everyday")
       {
            $delivery_start_date = $request->delivery_start_date;
            $delivery_end_date = $request->delivery_end_date;
       }
        
        $time_slot= $request->time_slot;
        
        $ar=address::where('user_id',$user_id)->where('select_status',1)->first();  

      /* $ar= DB::table('address')
            ->select('society','city','house_no','landmark','address_id','state','pincode')
            ->where('user_id', $user_id)
            ->where('select_status', 1)
            ->first();*/
       $address_id=$ar->address_id;
       $full_address=$ar->house_no." ".$ar->society." ".$ar->city." , landmark: ".$ar->landmark." , state-".$ar->state." , pincode : ".$ar->pincode;
       if(!$ar){
           	$message = array('status'=>'0', 'message'=>'Select any Address');
        	return $message;
       }
   
    
      $sub_successed=new subscription;
      $sub_successed->user_id=$user_id;
      $sub_successed->store_id=$store_id;
      $sub_successed->product_id=$product_id;
      $sub_successed->product_quantity=$product_quantity;
      $sub_successed->repeat_on=$repeat_on;
      $sub_successed->address=$full_address;
      $sub_successed->subscription_date=$current;
      $sub_successed->delivery_start_date=$delivery_start_date;
      $sub_successed->delivery_end_date=$delivery_end_date;
      $sub_successed->time_slot=$time_slot;
      $sub_successed->save();

        $sub_successed =subscription::orderBy('sub_id','desc')->first();
        	$message = array('status'=>'1', 'message'=>'Subscription detail added', 'data'=>$sub_successed );
        	return $message;
       
      
    }
    
    public function subscription_payment(Request $request)
    {
        $current = Carbon::now();
        $user_id= $request->user_id;
     
        $sub_details=subscription::where('user_id',$user_id)->where('sub_payment_generate_status','0')->with(['product','store','store_products'])->get();
      /*  $sub_details=subscription::
                    join('product', 'subscription.product_id', '=', 'product.product_id')
                    ->join ('product_varient', 'subscription.product_id', '=', 'product_varient.product_id')
                    ->join('store_products','subscription.store_id','=','store_products.store_id')
					
                    ->where('user_id',$user_id)
                    ->first();

                   
*/


        
        
        $price2=0;
        $price5=0;
        $no_of_days=0;
        $no_of_payment_generated=0;
      /*   if(count($sub_details)>0)
        {
            
               */ 
                 
            foreach($sub_details as $sub_detail)
            {

                  $sub_id=$sub_detail->sub_id;
                  $user_id=$sub_detail->user_id;
                  $store_id=$sub_detail->store_id;
                  $product_id=$sub_detail->product_id;
                 // $product_quantity=$sub_detail->product_quantity;
                  $repeat_on=$sub_detail->repeat_on;
                  $delivery_start_date=$sub_detail->delivery_start_date;
                  $delivery_end_date=$sub_detail->delivery_end_date;
                  $mrp=$sub_detail->store_products->mrp;
                  $qty=$sub_detail->product_quantity;
				   
                 
                    if($repeat_on=="one time delivery")
                    {
                        if($current==$delivery_end_date)
                        {
                            $no_of_days=1;
                        
							$price_of_singleday=$mrp*$qty;
							
							
							$delcharge=freedeliverycart::
											where('id', 1)
											->first();
									   
							
							
							if ($delcharge->min_cart_value<=$price_of_singleday){
								$charge=0;
							}  
							else{
								$charge =$delcharge->del_charge;
							}
				        $price_of_singleday_with_delivery=$price_of_singleday+$charge;
				        $total_price=$no_of_days*$price_of_singleday_with_delivery;
							
							
                        }
                    }
                    else if($repeat_on=="Everyday")
                    {
				
							
                        $nextday = date('Y-m-d',strtotime($delivery_end_date . "+1 days"));
                        
                        $current=date('Y-m-d',strtotime($current));
                        
					
                      //if($current==$nextday)
                        //{
						
                        $delivery_start_date = Carbon::parse($delivery_start_date);
						$delivery_end_date = Carbon::parse($delivery_end_date);
						$no_of_days = $delivery_end_date->diffInDays($delivery_start_date);
                      
                        $no_of_days=$no_of_days+1;
                        $price_of_singleday=$mrp*$qty;
						
						$delcharge=freedeliverycart::
											where('id', 1)
											->first();
									   
						
                                if ($delcharge->min_cart_value<=$price_of_singleday)
                                {
                                        $charge=0;
                                }  
                                else
                                {

                                        $charge =$delcharge->del_charge;
                                }
                                
                                $price_of_singleday_with_delivery=$price_of_singleday+$charge;
                                    
                                $total_price=$no_of_days*$price_of_singleday_with_delivery;
                                    
							
                           
                        //}

                      
                             
                     
                    }
					$sub_pay = new subscription_payment;
					$sub_pay->sub_id=$sub_id;
                    $sub_pay->user_id=$user_id;
                    $sub_pay->store_id=$store_id;
					$sub_pay->subscription_days=$no_of_days;
					$sub_pay->price_of_singleday=$price_of_singleday;
					$sub_pay->one_day_delivery_charge=$charge;
					$sub_pay->one_day_price_with_delivery=$price_of_singleday_with_delivery;
                    $sub_pay->total_price=$total_price;
                    $sub_pay->delivery_start_date=$delivery_start_date;
                    $sub_pay->delivery_end_date=$delivery_end_date;
                    $sub_pay->save();
                    $cnt=$sub_pay->count();
                  
                    if($cnt>0)
                    {
                        $no_of_payment_generated=$no_of_payment_generated+1;
                        $subscription_update=subscription::find($sub_id);
                        $subscription_update->sub_payment_generate_status='1';
                        $subscription_update->save();

                    }
              
              
                }

               // $subscriptionpaymentsuccessed =subscription::find($sub_id)->subscription_payment;
                $message = array('status'=>'1', 'message'=>'payment successed', 'data'=>$no_of_payment_generated );
                return $message;  
            
        
       /*     
        }
        else
        {
        
            $message = array('status'=>'1', 'message'=>'No Subscription found', 'data'=>$sub_id );
         return $message;
        } */
       
    }
}
