<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subscription extends Model
{
    protected $table="subscription";
    protected $primaryKey='sub_id';
    //
  
    public function subscription_payment()
    {
        return $this->hasOne('App\subscription_payment','sub_id');
    }

  public function product()
    {
        return $this->belongsTo('App\Product','product_id');
    }
 
    public function store()
    {
        return $this->belongsTo('App\store','store_id');
    }

    
    public function store_products()
    {
        return $this->belongsTo('App\store_products','store_id');
    }

  



    
}
